Pod::Spec.new do |s|
  s.name      = 'BDBOAuth1Manager'
  s.version   = '1.7.2'
  s.license   = 'MIT'
  s.summary   = 'AFNetworking 4.0-compatible replacement for AFOAuth1Client.'
  s.homepage  = 'https://bitbucket.org/fsadmin/bdboauth1manager'
  s.social_media_url = 'https://twitter.com/bradbergeron'
  s.authors   = { 'Bradley David Bergeron' => 'brad@bradbergeron.com' }
  s.source    = { :git => 'https://bitbucket.org/fsadmin/bdboauth1manager.git', :tag => s.version.to_s }
  s.requires_arc = true

  s.ios.deployment_target = '13.0'
  s.osx.deployment_target = '10.15'

  s.source_files = 'BDBOAuth1Manager/**/*.{h,m}'

  s.dependency 'AFNetworking/NSURLSession', '~> 4.0.0'
end
